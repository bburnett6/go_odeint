
package odeint

import (
	_ "fmt"
	"math"
)

//##################################
// Predictor Corrector BDF2 Stepper
//##################################

type bdf2 struct {
	ynm1 []float64
	isInitialized bool
	tolerance float64
	maxIterations int 

	rhs func(float64, []float64) []float64
}

func (b *bdf2) Step(t float64, h float64, yn *[]float64, ynp1 *[]float64) {
	tmp := make([]float64, len((*yn)))
	ynp1_old := make([]float64, len((*yn)))
	var e,etmp float64
	err := 10.0
	var p int

	if !b.isInitialized {
		//initialize the bdf2 with a bwd euler
		b.ynm1 = make([]float64, len((*yn)))

		//predict with fwd euler
		tmp = b.rhs(t + h, (*yn))
		for i := 0; i < len(tmp); i++ {
			ynp1_old[i] = (*yn)[i] + h * tmp[i]
		}
		//correct with semi-implicit bwd euler fixed point iteration
		for err > b.tolerance && p < b.maxIterations {
			//compute semi-implicit step
			tmp = b.rhs(t + h, ynp1_old)
			for i := 0; i < len((*ynp1)); i++ {
				(*ynp1)[i] = (*yn)[i] + h * tmp[i]
			}
			//calculate error
			etmp = 0.0
			for i := 0; i < len(tmp); i++ {
				e = ynp1_old[i] - (*ynp1)[i]
				etmp += e * e 
			}
			err = math.Sqrt(etmp)
			//swap old and new for next iteration
			for i := 0; i < len(tmp); i++ {
				ynp1_old[i] = (*ynp1)[i]
			}
			p++
		}
		b.isInitialized = true

	} else {
		//safe to perform the bdf2

		//predict with fwd euler
		tmp = b.rhs(t + h, (*yn))
		for i := 0; i < len(tmp); i++ {
			ynp1_old[i] = (*yn)[i] + h * tmp[i]
		}
		//correct with semi-implicit bdf2 fixed point iteration
		for err > b.tolerance && p < b.maxIterations {
			//compute semi-implicit step
			tmp = b.rhs(t + h, ynp1_old)
			for i := 0; i < len((*ynp1)); i++ {
				(*ynp1)[i] = 4.0/3.0 * (*yn)[i] 
				(*ynp1)[i] += -1.0/3.0 * b.ynm1[i] 
				(*ynp1)[i] += 2.0/3.0 * h * tmp[i]
			}
			//calculate error
			etmp = 0.0
			for i := 0; i < len(tmp); i++ {
				e = ynp1_old[i] - (*ynp1)[i]
				etmp += e * e 
			}
			err = math.Sqrt(etmp)
			//swap old and new for next iteration
			for i := 0; i < len(tmp); i++ {
				ynp1_old[i] = (*ynp1)[i]
			}
			p++
		}

	}

	//step
	for i := 0; i < len((*yn)); i++ {
		b.ynm1[i] = (*yn)[i]
		(*yn)[i] = (*ynp1)[i]
	}

}

func NewBDF2(tol float64, rhs func(float64, []float64) []float64) *bdf2 {

	b := bdf2{
		isInitialized: 	false,
		tolerance: 		tol,
		maxIterations: 	50,

		rhs: 			rhs,
	}

	return &b
}