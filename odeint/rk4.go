
package odeint

import (
	_ "fmt"
	_ "math"
)

//#######################
// RK4 Stepper
//#######################

type rk4 struct {
	k1 []float64
	k2 []float64
	k3 []float64
	k4 []float64

	rhs func(float64, []float64) []float64
}

func (r *rk4) Step(t float64, h float64, yn *[]float64, ynp1 *[]float64) {
	tmp := make([]float64, len((*yn)))

	r.k1 = r.rhs(t, (*yn))

	for i := 0; i < len(tmp); i++ {
		tmp[i] = (*yn)[i] + h * r.k1[i] / 2.0
	}
	r.k2 = r.rhs(t + h/2.0, tmp)

	for i := 0; i < len(tmp); i++ {
		tmp[i] = (*yn)[i] + h * r.k2[i] / 2.0
	}
	r.k3 = r.rhs(t + h/2.0, tmp)

	for i := 0; i < len(tmp); i++ {
		tmp[i] = (*yn)[i] + h * r.k3[i]
	}
	r.k4 = r.rhs(t + h, tmp)

	for i := 0; i < len((*ynp1)); i++ {
		(*ynp1)[i] = (*yn)[i]
		(*ynp1)[i] += 1.0/6.0 * (r.k1[i] + 2.0*r.k2[i] + 2.0*r.k3[i] + r.k4[i]) * h 
	}

	for i := 0; i < len((*yn)); i++ {
		(*yn)[i] = (*ynp1)[i]
	}
}

func NewRK4(rhs func(float64, []float64) []float64) *rk4 {

	r := rk4{
		rhs: rhs,
	}

	return &r 
}