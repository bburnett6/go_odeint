
package odeint

import (
	"math"
)

var (
	//machine epsilon in go
	//see: https://stackoverflow.com/questions/22185636/easiest-way-to-get-the-machine-epsilon-in-go
	MachineEpsilon = math.Nextafter(1, 2) - 1
)