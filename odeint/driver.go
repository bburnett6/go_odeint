
package odeint

import (
	"fmt"
	"math"
	"os"
	"strings"
)

type integrator interface {
	Integrate()
	Result()
	solutionAsString()
}

type odeint struct {
	f *os.File
	writeEvery int 
	delimiter string
	ti, tf float64 //initial and final time
	ynp1 []float64
	yn []float64
	s Stepper //stepper to use. rk4, bdf2, etc.
	nstep int //number of steps in t
	t, h float64 //current t value, step size in t
	debug bool
	hasSolution bool
}

func NewOdeint(
	ti float64,
	tf float64,
	ystart []float64,
	nstep int,
	s Stepper,
	f *os.File,
	writeEvery int,
) *odeint {

	odei := odeint{
		f:			f,
		writeEvery:	writeEvery,
		delimiter:	",",
		ti: 		ti,
		tf: 		tf,
		ynp1:		make([]float64, len(ystart)),
		yn:			ystart,
		s: 			s,
		nstep: 		nstep,
		t: 			ti,
		h: 			math.Abs(tf - ti) / float64(nstep),
		hasSolution: false,
		debug:		false,
	}

	return &odei
}

func (ode *odeint) Integrate() {

	if ode.writeEvery > 0 {
		fmt.Fprintf(ode.f, "%s\n", ode.solutionAsString())
	}

	for n := 0; n < ode.nstep; n++ {
		if n == ode.nstep - 1 { //final step make sure step size doesn't under/over shoot
			ode.h = ode.tf - ode.t
			if ode.debug {
				fmt.Println("Final time step size:", ode.h)
			}
		}

		if ode.debug {
			fmt.Println("step", n, "time", ode.t)
			fmt.Println("yn", ode.yn)
		}
		
		ode.s.Step(ode.t, ode.h, &ode.yn, &ode.ynp1)
		ode.t += ode.h 

		if ode.writeEvery != 0 && n % ode.writeEvery == 0 {
			fmt.Fprintf(ode.f, "%s\n", ode.solutionAsString())
		}

		if ode.debug {
			fmt.Println("ynp1", ode.ynp1)
		}
	}
	if ode.debug {
		fmt.Println("final time", ode.t)
	}
	if ode.writeEvery > 0 {
		fmt.Fprintf(ode.f, "%s\n", ode.solutionAsString())
	}

	ode.hasSolution = true

}

func (ode odeint) Result() ([]float64, error) {

	if ode.hasSolution {
		return ode.ynp1, nil
	} else {
		return ode.yn, fmt.Errorf("No solution. Try integrating first")
	}
}

func (ode odeint) solutionAsString() string {
	l := len(ode.yn) + 1 
	s := make([]string, l)

	s[0] = fmt.Sprintf("%f", ode.t) 
	for i := 0; i < len(ode.yn); i++ {
		s[i+1] = fmt.Sprintf("%f", ode.yn[i])
	}

	return strings.Join(s, ode.delimiter)
}