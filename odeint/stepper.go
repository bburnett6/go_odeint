
package odeint

type Stepper interface {
	Step(float64, float64, *[]float64, *[]float64)
}

