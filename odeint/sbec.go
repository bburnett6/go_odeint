
package odeint

import (
	"fmt"
	_ "math"

	"gonum.org/v1/gonum/mat"
)

//########################################################
// Semi Implicit Backwards Euler with corrections Stepper
//########################################################

type sbec struct {
	corrections int

	rhs func(float64, []float64) []float64
	j func(float64, []float64) []float64
}

func (b *sbec) Step(t float64, h float64, yn *[]float64, ynp1 *[]float64) {
	l := len((*yn))
	matI := eye(l)

	//get F and J
	fn := mat.NewDense(l, 1, b.rhs(t + h, (*yn)))
	jn := mat.NewDense(l, l, b.j(t + h, (*yn)))
	//apply I - h * J, for scalar h
	jn.Apply(func(i, j int, v float64) float64 { return matI.At(i, j) - h * v }, jn) 
	//compute [I - h*J] / F
	var x mat.Dense 
	err := x.Solve(jn, fn)
	if err != nil {
		fmt.Println("Oh no! Solve is not working...")
	}
	tmp := x.RawMatrix().Data //get result as []float64 and store in tmp

	//perform the semi implicit step
	for i := 0; i < len(tmp); i++ {
		(*ynp1)[i] = (*yn)[i] + h * tmp[i]
	}
	//perform corrections
	for c := 0; c < b.corrections; c++ {
		tmp = b.rhs(t + h, (*yn))
		for i := 0; i < len(tmp); i++ {
			(*ynp1)[i] = (*yn)[i] + h * tmp[i]
		}
	}

	//step
	for i := 0; i < len((*yn)); i++ {
		(*yn)[i] = (*ynp1)[i]
	}
}

func NewSbec(corrs int, rhs func(float64, []float64) []float64, j func(float64, []float64) []float64) *sbec {

	b := sbec{
		corrections:	corrs,

		rhs:			rhs,
		j:				j,
	}

	return &b
}