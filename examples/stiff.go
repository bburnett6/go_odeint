
package main 

import (
	"fmt"
	"math"
	"os"
	"strconv"

	"gitlab.com/bburnett6/go_odeint/odeint"
)

func stiff_rhs() func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t //problem doesn't depend on t. Function signiture should still require t

		u := make([]float64, 2)
		u[0] =  998.0 * y[0] + 1998.0 * y[1]
		u[1] = -999.0 * y[0] - 1999.0 * y[1]

		return u 
	}

}

func stiff_j() func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t 

		u := make([]float64, 4)
		u[0] = 998.0
		u[1] = 1998.0
		u[2] = -999.0
		u[3] = -1999.0

		return u
	}
}

func main() {

	var stiffStepper odeint.Stepper
	switch os.Args[1] {
	case "rk4":
		stiffStepper = odeint.NewRK4(stiff_rhs())
	case "pcbe":
		stiffStepper = odeint.NewPcbe(odeint.MachineEpsilon * 1.01, stiff_rhs())
	case "sbe":
		stiffStepper = odeint.NewSbe(stiff_rhs(), stiff_j())
	case "sbec":
		stiffStepper = odeint.NewSbec(1, stiff_rhs(), stiff_j())
	case "bdf2":
		stiffStepper = odeint.NewBDF2(odeint.MachineEpsilon * 1.01, stiff_rhs())
	default:
		fmt.Println("Unknown method", os.Args[1], "defaulting to sbe")
		stiffStepper = odeint.NewSbe(stiff_rhs(), stiff_j())
	}

	initialCondition := []float64{1.0, 0.0}
	ti := 0.0
	tf := 1.0
	nsteps, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Println("Timestep funky:", os.Args[2], "defaulting to 100")
		nsteps = 100
	}

	stiff := odeint.NewOdeint(ti, tf, initialCondition, nsteps, stiffStepper, os.Stdout, 0)

	stiff.Integrate()

	res, err := stiff.Result()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Solver Solution:", res)
	}
	uexact := make([]float64, 2)
	uexact[0] = 2.0 * math.Exp(-tf) - math.Exp(-1000.0 * tf)
	uexact[1] = - math.Exp(-tf) - math.Exp(-1000.0 * tf)
	fmt.Println("Exact Solution", uexact)
}