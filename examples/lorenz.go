
package main 

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/bburnett6/go_odeint/odeint"
)

func lorenz_rhs(sigma float64, rho float64, beta float64) func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t //problem doesn't depend on t. Function signiture should still require t

		u := make([]float64, 3)
		u[0] = sigma * (y[1] - y[0])
		u[1] = y[0] * (rho - y[2]) - y[1]
		u[2] = y[0] * y[1] - beta * y[2]

		return u 
	}

}

func lorenz_j(sigma float64, rho float64, beta float64) func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t 

		u := make([]float64, 9)
		u[0] = -sigma
		u[1] = sigma
		u[2] = 0.0
		u[3] = rho - y[2]
		u[4] = -1
		u[5] = -y[0]
		u[6] = y[1]
		u[7] = y[0]
		u[8] = -beta

		return u
	}
}

func main() {

	rho := 28.0
	sigma := 10.0
	beta := 8.0 / 3.0

	var lorenzStepper odeint.Stepper
	switch os.Args[1] {
	case "rk4":
		lorenzStepper = odeint.NewRK4(lorenz_rhs(sigma, rho, beta))
	case "pcbe":
		lorenzStepper = odeint.NewPcbe(odeint.MachineEpsilon * 1.01, lorenz_rhs(sigma, rho, beta))
	case "sbe":
		lorenzStepper = odeint.NewSbe(lorenz_rhs(sigma, rho, beta), lorenz_j(sigma, rho, beta))
	case "bdf2":
		lorenzStepper = odeint.NewBDF2(odeint.MachineEpsilon * 1.01, lorenz_rhs(sigma, rho, beta))
	default:
		fmt.Println("Unknown method", os.Args[1], "defaulting to sbe")
		lorenzStepper = odeint.NewSbe(lorenz_rhs(sigma, rho, beta), lorenz_j(sigma, rho, beta))
	}

	initialCondition := []float64{1.0, 1.0, 1.0}
	ti := 0.0
	tf := 40.0
	nsteps, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Println("Timestep funky:", os.Args[2], "defaulting to 100")
		nsteps = 100
	}

	f, err := os.Create("lorenz.csv")
	if err != nil {
		fmt.Println("Oh no file opening failed!")
		f = os.Stdout
	}
	//f := os.Stdout
	defer f.Close()
	lorenz := odeint.NewOdeint(ti, tf, initialCondition, nsteps, lorenzStepper, f, 5)

	lorenz.Integrate()

	res, err := lorenz.Result()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Solver Solution:", res)
	}
	
}