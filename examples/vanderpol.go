
package main 

import (
	"fmt"
	"os"

	"gitlab.com/bburnett6/go_odeint/odeint"
)

func vanderpol_rhs(alpha float64) func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t //vanderpol doesn't depend on t. Function signiture should still require t

		u := make([]float64, 2)
		u[0] = y[1]
		u[1] = alpha * (1.0 - y[0] * y[0]) * y[1] - y[0]

		return u 
	}

}

func vanderpol_j(alpha float64) func(float64, []float64) []float64 {

	return func(t float64, y []float64) []float64 {
		_ = t 

		j := make([]float64, 4)
		j[0] = 1 
		j[1] = 0
		j[2] = - 2 * alpha * y[0] * y[1] - 1 
		j[3] = alpha * (1 - y[0] * y[0])

		return j 
	}
}

func main() {
	//vdpStepper := odeint.NewRK4(vanderpol_rhs(1.0))
	//vdpStepper := odeint.NewBDF2(odeint.MachineEpsilon * 1.01, vanderpol_rhs(1.0))
	//vdpStepper := odeint.NewPcbe(odeint.MachineEpsilon * 1.01, vanderpol_rhs(1.0))
	//vdpStepper := odeint.NewSbe(vanderpol_rhs(1.0), vanderpol_j(1.0))
	vdpStepper := odeint.NewSbec(3, vanderpol_rhs(1.0), vanderpol_j(1.0))
	initialCondition := []float64{2.0, 0.0}

	//NewOdeint(ti, tf, initialCondition, nsteps, steppingAlgo, outputFile, writeEveryNSteps)
	vdp := odeint.NewOdeint(0.0, 1.0, initialCondition, 10, vdpStepper, os.Stdout, 0)

	vdp.Integrate()

	res, err := vdp.Result()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Solution:", res)
	}
}